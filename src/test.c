#define _DEFAULT_SOURCE
#include "test.h"
#include <stdio.h>

inline void print_err(int test_case) {fprintf(stderr, "test case %d failed! \n", test_case);};
inline void print_success(int test_case) {printf("test case %d success! \n", test_case);};

void free_heap(void* heap, size_t sz) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = sz}).bytes);
}
void test_1() {
    void* heap = heap_init(10000);
    if(!heap) print_err(1);
    debug_heap(stdout, heap);
    void* normal_test = _malloc(200);
    if(!normal_test) print_err(1); 
    debug_heap(stdout, heap);
    _free(normal_test);
    debug_heap(stdout, heap);
    free_heap(heap, 10000);
    print_success(1);
}

void test_2() {
    void* heap = heap_init(10000);
    if(!heap) print_err(2);
    debug_heap(stdout, heap);
    void* block_1 = _malloc(300);
    debug_heap(stdout, heap);
    void* block_2 = _malloc(300);
    debug_heap(stdout, heap);
    if(!block_2|| !block_1) print_err(2);
    _free(block_2);
    debug_heap(stdout, heap);
    free_heap(heap, 10000);
    print_success(2);
}

void test_3() {
    void* heap = heap_init(10000);
    if(!heap) print_err(3);
    debug_heap(stdout, heap);
    void* block_1 = _malloc(300);
    debug_heap(stdout, heap);
    void* block_2 = _malloc(300);
    debug_heap(stdout, heap);
    void* block_3 = _malloc(300);
    debug_heap(stdout, heap);
    if(!block_1 || !block_2 || !block_3) 
        print_err(3);
    _free(block_3);
    debug_heap(stdout, heap);
    _free(block_2);
    debug_heap(stdout, heap);
    print_success(3);
    free_heap(heap, 10000);
}

void test_4 () {
    void* heap = heap_init(10000);
    if(!heap) print_err(4);
    debug_heap(stdout, heap);
    void* block_1 = _malloc(9000);
    debug_heap(stdout, heap);
    void* block_2 = _malloc(3000);
    debug_heap(stdout, heap);
    if(!block_1 || !block_2 || block_1 + 9000 != block_2)
        print_err(4);
    _free(block_2);
    debug_heap(stdout, heap);
    _free(block_1);
    debug_heap(stdout, heap);
    free_heap(heap, 10000);
    print_success(4);
}

void test_5() {
    size_t full_size = capacity_from_size((block_size) {REGION_MIN_SIZE}).bytes;

    void* heap = heap_init(full_size);
    void* block_1 = _malloc(full_size);
    debug_heap(stdout, heap);
    if (!heap || !block_1) {
        print_err(5);
    }

    debug_heap(stdout, heap);

    void* p = mmap(block_1 + full_size, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    if (p == MAP_FAILED) {
        print_err(5);
    }

    void* block_2 = _malloc(full_size);

    debug_heap(stdout, heap);

    if (!block_2) {
        print_err(5);
    }

    // Free allocated space
    _free(block_1);
    _free(block_2);
    free_heap(heap, full_size);

    print_success(5);
}
