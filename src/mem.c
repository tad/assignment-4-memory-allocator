#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

// return (block_size) {cap.bytes + offsetof( struct block_header, contents ) }
extern inline block_size size_from_capacity( block_capacity cap );

//return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) }
extern inline block_capacity capacity_from_size( block_size sz );
// 
static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

// initial new block 
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

// if query < REGION_MIN_SIZE then return REGION_MIN_SIZE, else return query 
static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

//map_pages: void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком 
addr: start address
query: capacity of block -> need to find size of block
----------------------
size = capacity + contents
region: all space that contain blocks, as heap ( address at the beginning of linked list)
block: [header + data], data comes after header
*/
static struct region alloc_region  ( void const * addr, size_t query ) {
  if(!addr) return REGION_INVALID;

  size_t region_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);

  void* region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
  if(region_addr == MAP_FAILED) region_addr = map_pages(addr, region_size, 0);

  /* if can't create region at specific addr, then return region invalid*/
  if(region_addr == MAP_FAILED) return REGION_INVALID;
  if(!region_addr) return REGION_INVALID;
    
  /*init new block*/
  block_init(region_addr, (block_size){.bytes = region_size}, NULL);

  return (struct region) {
      .addr = region_addr,
      .size = region_size,
      .extends = region_addr == addr
  };

}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block->is_free &&query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static void set_block_capacity(struct block_header* block, size_t sz) {
  block->capacity.bytes = sz;
}

/*what is too big? - */
static bool split_if_too_big( struct block_header* block, size_t query ) {
if(!block || !block_splittable(block, query)) return false;

  void* new_block_addr = block->contents + query;
  block_init(new_block_addr, (block_size){block->capacity.bytes-query}, block->next);
  block->next = new_block_addr;
  block->capacity.bytes = query;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if(!block|| !block->next || !mergeable(block, block->next)) return false;
  size_t new_size = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
  set_block_capacity(block, new_size);
  block->next = block->next->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

/* what if this is a round linked list? */
static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {

  if(!block)
    return (struct block_search_result){.type = BSR_CORRUPTED, .block = NULL};
  struct block_header* cnt = block;
  while(cnt){
      while ( try_merge_with_next(cnt));
      if (cnt->is_free && block_is_big_enough(sz, cnt)){
        return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = cnt};
      }

    if(!cnt->next) 
      return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block =cnt};
    // if( cnt->next == cnt || cnt->next == block) return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL};
    if( cnt->next == cnt || cnt->next == block) break;
    cnt = cnt->next;
  }
  return (struct block_search_result){
    .type = BSR_REACHED_END_NOT_FOUND, 
    .block = cnt
  };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

  struct block_search_result result = find_good_or_last(block, query);
  if (result.type != BSR_FOUND_GOOD_BLOCK) {
    return result;
  }
  split_if_too_big(result.block, query);
  result.block->is_free = false;
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    if(!last) return NULL;
    struct region new_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&new_region)) {
        return NULL;
    }

    last->next = new_region.addr;
    if(new_region.extends && try_merge_with_next(last)) return last;
    return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result bsr = try_memalloc_existing(query, heap_start);

  while (bsr.type != BSR_FOUND_GOOD_BLOCK && bsr.block != NULL) {
    if (!grow_heap(bsr.block, query)) return NULL;
    bsr = try_memalloc_existing(query, heap_start);
  }

  return bsr.block;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header));
}
